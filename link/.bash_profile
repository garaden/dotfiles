if [ -f ~/.bashrc ]; then
    source ~/.bashrc
fi
test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"

# Added by nex: https://git.hubteam.com/HubSpot/nex
. ~/.hubspot/shellrc
