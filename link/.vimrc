" Vundle boilerplate
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim/
call vundle#begin()

Plugin 'gmarik/Vundle.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'tpope/vim-repeat'
Plugin 'tpope/vim-unimpaired'
Plugin 'groenewege/vim-less'
Plugin 'vim-syntastic/syntastic'

call vundle#end()
filetype plugin indent on

set ignorecase
set smartcase
set showcmd
set ts=4 sw=4 sts=4
set expandtab
set listchars=tab:>-
syntax on
autocmd Filetype ruby setlocal ts=2 sts=2 sw=2
autocmd Filetype javascript setlocal ts=2 sts=2 sw=2
set visualbell
set noerrorbells
nmap <leader>ne :NERDTreeToggle<cr>
set backspace=indent,eol,start
let g:syntastic_javascript_checkers = ['eslint']
